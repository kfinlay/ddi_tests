# Questions about DDI

## Some background on families of data

We're trying to develop a system that can handle metadata export for different types of data collections:

 - Surveys with two or more core tables per collection event
   + ACS household and person
 - Administrative records received over many years
   + Medicaid enrollment (MSIS)
     * We received data once a year in twenty files (having the exact same structure)
     * The list of variables changed a little over time 
     * Most users would probably like to turn the data into panel
 - Administrative records received from many agencies, but sharing a concept
   + SNAP enrollment from state agencies
     * Time periods vary
     * Variable lists are different, although there is probably a lot of conceptual overlap because of federal enrollment guidelines
     * Most users would probably like to turn the data into panel

For the two adrec examples, are these as simple as nesting StudyUnits inside Groups?

## Some specific problems I am having

 - Validation issues
   + Can't load XML name space properly; xml:lang not recognized during validation
   + LogicalProduct is rejected as element of StudyUnit; the spec discusses substitution for BaseLogicalProduct, but I don't understand what that means
   + Do I really need URNs all over the place?
   + '''Element '{ddi:reusable:3_2}NoteContent': This element is not expected. Expected is one of ( {ddi:reusable:3_2}TypeOfNote, {ddi:reusable:3_2}NoteSubject, {ddi:reusable:3_2}Relationship ).''' I see NoteContent as allowed for Note, so I didn't understand this error.
 - I've tried transforming the XML using XSLT sheets with no luck. That seemed like the easiest way to present the DDI to people who didn't want to read XML
 - How to store variable codes
   + I was thinking of having a database of all collections so I could keep track of code scheme reuse and manage URNs. Is that overthinking it?
   + Where do codes go and how are they referenced? (Just FYI, we don't currently plan to release descriptive statistics, but coding schemes.)
   + How do you split up variables by table (e.g., household vs. person)
     * How do you designate the table-linking keys?
   + How do you map variable inventories to files on disk?